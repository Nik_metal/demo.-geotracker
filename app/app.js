'use strict';

//Настройки
let appSettings = {
    defaultLocale: "ru"
};

// Declare app level module which depends on views, and components
angular.module('geoTrackerApp', [
    'ngRoute',
    'geoTrackerApp.mainModule',
    'pascalprecht.translate',
    'ui.bootstrap',
    'ngLocale',
    'ngMaterial',
    'material.components.expansionPanels',
    'ngMessages',
    'ngMaterialDatePicker',
    'ngAria',
    'ngAnimate',
    'dx'
])
    .config(['$locationProvider', '$translateProvider', '$routeProvider', function ($locationProvider,
                                                                                    $translateProvider) {
        //TODO: need to move translations to resources
        $translateProvider.translations('ru', {
            common: {
                appTitle: "Демо. Геолокация мобильного абонента",
                requestFailure: "Ошибка выполнения запроса"
            },
            loginView: {
                title: 'Авторизация',
                login: "Логин",
                password: "Пароль",
                loginAction: "Вход",
                loginRequiredString: "Введите логин.",
                passwordRequiredString: "Введите пароль.",
                authFailure: "Неверный логин или пароль",
                authRequestError:"Ошибка запроса авторизации"
            },
            trackerView:{
                mobileNumber:"Номер телефона",
                loginRequiredString: "Введите номер.",
                searchAction: "Найти",
                trackAction: "Перемещение",
                fromTime:"Начало",
                toTime:"Окончание",
                pointQuantity:"Кол-во пунктов"
            }
        });
        $translateProvider.translations('en', {
            common: {
                appTitle: "Demo. Mobile subscriber geo-location",
                requestFailure: "Request error"
            },
            loginView: {
                title: 'Authorization',
                login: "Login",
                password: "Password",
                loginAction: "Login",
                loginRequiredString: "Login required.",
                passwordRequiredString: "Password required.",
                authFailure: "Wrong username or password",
                authRequestError:"Authenticate request error"
            },
            trackerView:{
                mobileNumber:"Mobile number",
                loginRequiredString: "Number required.",
                searchAction: "Find",
                trackAction: "Track",
                fromTime:"From",
                toTime:"To",
                pointQuantity:"Point count"
            }
        });

        $translateProvider.preferredLanguage(appSettings.defaultLocale);
        $locationProvider.hashPrefix('!');

        getConfig().then(function (settings) {
            appSettings = Object.assign(appSettings, settings);
        });
    }])
    .constant('AppConstants', {
        // Here global app constants
    });

/**
 * Summary. (loads application settings)
 * @return {object} application settings.
 */
function getConfig() {
    return new Promise(function (resolve) {
        $.getJSON("assets/appConfig.json")
            .then(function (appConfig) {
                const keys = Object.keys(appConfig);
                let settings = {};
                for (const key of keys) {
                    settings[key] = appConfig[key];
                }
                resolve(settings);
            });
    });
}


angular.element(document).ready(function () {
    let locale = appSettings.defaultLocale;
    Promise.all([
        // Core
        fetch('assets/localization/supplemental/likelySubtags.json'),

        // Date
        fetch('assets/localization/main/' + locale + '/ca-gregorian.json'),
        fetch('assets/localization/main/' + locale + '/timeZoneNames.json'),
        fetch('assets/localization/supplemental/timeData.json'),
        fetch('assets/localization/supplemental/weekData.json'),

        // Number
        fetch('assets/localization/main/' + locale + '/numbers.json'),
        fetch('assets/localization/supplemental/numberingSystems.json')
    ])
        .then(function (responses) {
            return Promise.all(responses.map(function (response) {
                return response.json();
            }));
        })
        .then(function (json) {
            Globalize.load(json);
        })
        .then(function () {
            Globalize.locale(locale);
        });

    DevExpress.localization.locale(locale);
});
