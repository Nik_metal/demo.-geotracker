'use strict';

angular.module('geoTrackerApp.mainModule', ['ngRoute', 'ngCookies', 'ngStorage'])
    .config(config)
    .run(run);

config.$inject = ['$routeProvider'];

function config($routeProvider) {
    $routeProvider
        .when('/tracker', {
            templateUrl: 'modules/views/tracker.html',
        })
        .when('/login', {
            templateUrl: 'modules/views/login.html'
        })
        .otherwise({redirectTo: '/login'});
}

run.$inject = ['$rootScope', '$location', 'loginService'];

function run($rootScope, $location, loginService) {
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        // redirect to login page if not logged in and trying to access a restricted page
        let restrictedPage = $.inArray($location.path(), ['/login']) === -1;

        if (restrictedPage && loginService.isLoginRequired()) {
            $rootScope.$applyAsync(function () {
                $location.path('/login');
            });
        }
    });
}