(function () {
    'use strict';

    class LoginController {
        constructor($rootScope,
                    $cookies,
                    $scope,
                    $filter,
                    $interval,
                    $mdDialog,
                    $location,
                    loginService) {
            'ngInject'

            let vm = this;
            vm.loginModel = {
                username: '',
                password: ''
            };
            vm.isLogging = false;

            vm.loadOptions = {
                shadingColor: "rgba(0,0,0,0.4)",
                position: {of: "#main-view"},
                showIndicator: true,
                showPane: true,
                shading: true,
                closeOnOutsideClick: false,
                bindingOptions: {
                    visible: "vm.isLogging",
                },
            };

            /**
             * Summary. (login action)
             */
            vm.login = function () {
                vm.isLogging = true;
                loginService.login(vm.loginModel).then(function (authResult) {
                    vm.isLogging = false;
                    $scope.$apply();
                    if (authResult.errorMsg) {
                        DevExpress.ui.notify($filter('translate')('loginView.authRequestError'), "error", 2000);
                    }
                    else if (authResult.isAuth) {
                        $scope.$applyAsync(function () {
                            // redirect to target
                            $location.path('/tracker');
                        });
                    }
                    else
                        DevExpress.ui.notify($filter('translate')('loginView.authFailure'), "error", 2000);
                })
            }
        }
    }

    angular.module('geoTrackerApp.mainModule')
        .controller('LoginController', LoginController);
})();