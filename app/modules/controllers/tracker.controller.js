(function () {
    'use strict';

    class TrackerController {
        constructor($scope,
                    $filter,
                    trackerService) {
            'ngInject'

            let vm = this;

            vm.mobileNumber = null;
            vm.fromTime = new Date();
            vm.fromTime.setDate(vm.fromTime.getDate() - 1);
            vm.toTime =  new Date();
            vm.pointQuantity = 10;

            let trackerMap = L.map('mapId').setView([55.60, 37.36], 8);

            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibmlrbWV0YWwiLCJhIjoiY2p0MWNzMjUxMDZpcTQ5cGd4aGYxcGJveiJ9.vyPE8MOvSSGrB8Sh8Tn4mQ', {
                attribution: $filter('translate')('common.appTitle'),
                maxZoom: 18,
                id: 'mapbox.streets',
                accessToken: 'your.mapbox.access.token'
            }).addTo(trackerMap);

            let circlesGroup = L.featureGroup();
            let polyLinesGroup = L.featureGroup();
            let markersGroup = L.featureGroup();
            trackerMap.addLayer(circlesGroup);
            trackerMap.addLayer(polyLinesGroup);
            trackerMap.addLayer(markersGroup);

            vm.find = function () {
                clearMap();
                if (!vm.mobileNumber)
                    return;
                trackerService.find(vm.mobileNumber).then(function (result) {
                    if (!result) {
                        DevExpress.ui.notify($filter('translate')('common.requestFailure'), "error", 2000);
                        return;
                    }
                    L.circle([result.Geo.Latitude, result.Geo.Longitude], {
                        color: 'red',
                        fillColor: '#f03',
                        fillOpacity: 0.35,
                        //TODO: Need to estimate Radius to set correctly current zoom
                        radius: result.Geo.Radius
                    }).addTo(circlesGroup);
                    trackerMap.flyTo([result.Geo.Latitude, result.Geo.Longitude], 12);
                });
            };

            vm.getTrack = function () {
                clearMap();
                if (!vm.mobileNumber || !vm.fromTime || !vm.toTime)
                    return;
                trackerService.getTrack(vm.mobileNumber, vm.fromTime, vm.toTime, vm.pointQuantity)
                    .then(function (result) {
                        if (!result) {
                            DevExpress.ui.notify($filter('translate')('common.requestFailure'), "error", 2000);
                            return;
                        }
                        // Handled error with NoDataInfo
                        if (result.NoDataInfo) {
                            DevExpress.ui.notify(result.NoDataInfo, "error", 2000);
                            return;
                        }
                        let latlngs = [];
                        //NOTE: is geo objects ordered by time?
                        if (result.Geo) {
                            result.Geo.forEach(geo => {
                                let latlng = [geo.Latitude, geo.Longitude];
                                latlngs.push(latlng);
                                //NOTE: uncomment this to show track markers
                               // L.marker(latlng).addTo(markersGroup);
                            });
                            let polyline = L.polyline(latlngs, {color: 'red'}).addTo(polyLinesGroup);
                            // zoom the map to the polyline
                            trackerMap.fitBounds(polyline.getBounds());
                        }
                    });
            };

            function clearMap() {
                circlesGroup.clearLayers();
                polyLinesGroup.clearLayers();
                markersGroup.clearLayers();
            }
        }
    }

    angular.module('geoTrackerApp.mainModule')
        .controller('TrackerController', TrackerController);
})();