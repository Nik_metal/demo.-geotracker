(function () {
    'use strict';

    class MainController {
        constructor($scope,
                    $interval,
                    $mdDialog,
                    $location,
                    $translate,
                    loginService) {
            'ngInject'

            let vm = this;
            vm.locales = [
                {
                    name: 'ru',
                    image: 'assets/images/locales/ru.png'
                },
                {
                    name: 'en',
                    image: 'assets/images/locales/en.png'
                }
            ];

            // default selected locale
            let currentLocale = vm.locales.find(l => l.name === appSettings.defaultLocale);
            vm.currentLocale = {
                get locale() {
                    return currentLocale;
                },
                set locale(value) {
                    if (value && currentLocale.name !== value.name) {
                        currentLocale = value;
                        $translate.use(currentLocale.name)
                    }
                }
            };

            vm.isFabOpen = false;

            vm.logoff = function () {
                loginService.logoff();
                $scope.$applyAsync(function () {
                    $location.path('/login');
                });
            }
        }
    }

    angular.module('geoTrackerApp.mainModule')
        .controller('MainController', MainController);
})();