(function () {
    'use strict';

    /**
     * Summary. (Authentication service to get access token)
     */
    class LoginService {
        constructor($rootScope, $http, $location, $cookies, $filter) {
            'ngInject'
            const authPath = "/login";
            const cookiesName = "AuthData";
            /**
             * Summary. (This method call login method)
             * @param {object} loginModel with username and password
             * @return {object} authentication result.
             */
            this.login = function (loginModel) {
                if (!loginModel || !loginModel.username || !loginModel.password)
                    return null;
                return new Promise(function (resolve) {
                    // Auth Cookies clearing
                    $cookies.remove(cookiesName);
                    $http({
                        method: 'POST',
                        url: appSettings.apiUrl + authPath,
                        data: loginModel
                    }).then(function successCallback(response) {
                        let authResult = {
                            isAuth: false,
                            errorMsg: null,
                        };
                        // request error
                        if (!response || !response.data) {
                            authResult.isAuth = false;
                            authResult.errorMsg = $filter('translate')('loginView.authRequestError');
                            resolve(authResult);
                        }
                        else {
                            authResult.isAuth = response.data.IsAuth;
                            //need to update cookies
                            let expires = new Date(moment.unix(response.data.ExpireTime));
                            $cookies.put(cookiesName, JSON.stringify(response.data), {
                                expires: expires
                            });
                            resolve(authResult);
                        }

                    }, function errorCallback(response) {
                        let authResult = {
                            isAuth: false,
                            errorMsg: $filter('translate')('loginView.authRequestError'),
                        };
                        resolve(authResult);
                    });
                })
            };

            /**
             * Summary. (Checks auth cookies for expiration and is auth)
             * @return {boolean} true if no need to login, false - no need to login.
             */
            this.isLoginRequired = function () {
                let geoAPIAuthData = this.getAuthFromCookies();
                return !(geoAPIAuthData && geoAPIAuthData.IsAuth && !this.isTokenExpired(geoAPIAuthData.ExpireTime));
            };

            /**
             * Summary. (Returns auth cookies)
             * @return {object} auth data.
             */
            this.getAuthFromCookies = function () {
                let geoAPIAuthCookies = $cookies.get(cookiesName);
                if (geoAPIAuthCookies) {
                    return JSON.parse(geoAPIAuthCookies);
                }
                return null;
            };

            /**
             * Summary. (Checks auth cookies time for expiration )
             * @param {numeric} expireTime in unix timestamp  to check
             * @return {boolean} true if expired, false - is valid.
             */
            this.isTokenExpired = function (expireTime) {
                let date = moment.unix(expireTime);
                return date.isSameOrBefore(moment());
            };

            /**
             * Summary. (Logoff and delete auth cookies )
             */
            this.logoff = function () {
                $cookies.remove(cookiesName);
            };

            /**
             * Summary. (Returns access token, if it expired - redirects to login  )
             * @return {string} access token.
             */
            this.getAccessToken = function () {
                if (this.isLoginRequired()) {
                    $rootScope.$applyAsync(function () {
                        $location.path('/login');
                    });
                    return null;
                }
                let geoAPIAuthData = this.getAuthFromCookies();
                if (geoAPIAuthData)
                    return geoAPIAuthData.Token;
                else
                    $rootScope.$applyAsync(function () {
                        $location.path('/login');
                    });
            }
        }
    }

    angular.module('geoTrackerApp.mainModule')
        .service('loginService', LoginService);
})();
