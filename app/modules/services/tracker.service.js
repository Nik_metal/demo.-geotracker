(function () {
    'use strict';

    /**
     * Summary. (Tracker service to get subscriber geo location)
     */
    class TrackerService {
        constructor($rootScope, $http, $cookies, $filter, loginService) {
            'ngInject'

            /**
             * Summary. (get last coordinates for msisdn)
             * @param {string} msisdn mobile subscriber number
             * @return {object} location object.
             */
            this.find = function (msisdn) {
                if (!msisdn)
                    return null;
                return new Promise(function (resolve) {
                    $http({
                        method: 'POST', // NOTE: Why this method POST?
                        //TODO: string.format may be instead concat here
                        url: appSettings.apiUrl + "/" + msisdn + "/last",
                        headers: {
                            'Authorization': loginService.getAccessToken()
                        }
                    }).then(function successCallback(response) {
                        // request error
                        if (!response || !response.data)
                            resolve(null);
                        else
                            resolve(response.data);

                    }, function errorCallback(response) {
                        resolve(null);
                    });
                })
            };

            /**
             * Summary. (get last coordinates for msisdn)
             * @param {string} msisdn mobile subscriber number
             * @param {Date} dateStart date from
             * @param {Date} dateStop date to
             * @param {int} pointQuantity point count to show track
             * @return {object} list of ordered geo location objects.
             */
            this.getTrack = function (msisdn, dateStart, dateStop, pointQuantity = 2) {
                if (!msisdn || !dateStart || !dateStop)
                    return null;
                let dateStartUnix = moment(dateStart).unix();
                let dateStopUnix = moment(dateStop).unix();
                return new Promise(function (resolve) {
                    $http({
                        method: 'POST', // NOTE: Why this method POST too?
                        //TODO: string.format may be instead concat here
                        url: appSettings.apiUrl + "/" + msisdn + "/track/" + dateStartUnix + "/" + dateStopUnix + "/" + pointQuantity,
                        headers: {
                            'Authorization': loginService.getAccessToken()
                        }
                    }).then(function successCallback(response) {
                        // request error
                        if (!response || !response.data)
                            resolve(null);
                        else
                            resolve(response.data);

                    }, function errorCallback(response) {
                        resolve(null);
                    });
                })
            };
        }
    }

    angular.module('geoTrackerApp.mainModule')
        .service('trackerService', TrackerService);
})();
